from flask import Flask,render_template,request

app = Flask(__name__)


@app.route('/',methods = ['POST','GET'])
def main():
	return render_template("index.html")

@app.route('/add',methods = ['POST','GET'])
def addExpenses():
	try:
		arr1 = []
		expression = request.form.get('expression')
		stack = []
		count = 0
		dic = {'+' : 'A','-':'S','*':'M','/':'D'}
		exp=list(expression)
		out = []
		for i in range(len(exp)-1):
			if len(stack) == 0 and exp[i] not in '+-/*' :
				stack.append(exp[i])
				out.append('L ' + exp[i])
			elif len(stack) == 0 and exp[i] in '+-/*':
				pass
			else:
				if exp[i] not in '+-/*' and len(stack) != 0:
					stack.pop()
					out.append(dic[exp[i+1]]+" "+exp[i])
					k = i+1
					op_count = 0
						
						# for getting operator count
					while k < len(exp):
						if exp[k] in '+-/*':
							op_count += 1
						else:
							break
						k += 1
							
					c = op_count
						
					while op_count >1:
						for m in range(i+2 , i+c+1):
							z = dic[exp[m]]
							if(z == "S"):
								out[-1] += " N"
								z = "A"
							out.append(z +" "+'$'+str(count))
							op_count -= 1

							count-=1
						
					count += 1
					out.append('ST '+'$'+str(count))
		if(len(out)==3):
			return render_template("output.html",arr = out,lenn = len(out))
		else:
			return render_template("output.html",arr = out[:-1],lenn = len(out)-1)
	except:
		return render_template("error.html")
		
app.run(debug=True)